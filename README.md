> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS 4368

## Marcus Mobley 

### Assignment # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - [List other assignment requirements here]

2. [A2 README.md](a2/README.md "My A2 README.md file")

	- Install MYSQL
	- Tomcat Develop and Deploy WebApp
	
3. [A3 README.md](a3/README.md "My A3 README.md file")

	- Create ERD based upon business rules 
	- Provide screenshot of completed ERD
	- Provide DB resource links
	
4. [A4 README.md](a4/README.md "My A4 README.md file")

	- Clone files
	- Server - side Validation
	- Using MVC

5. [A5 README.md](a5/README.md "My A5 README.md file")
	
	- Clone files
	- Server - side Validation
	- Using MVC
	- Data Driven Application
	

6. [P1 README.md](p1/README.md "My P1 README.md file")

	- Clone files 
	- Client Form Validation
	- Modify index.jsp
	
7. [P2 README.md](p2/README.md "My P2 README.md file")

	- MVC Framework
	- Client-side server validation
	- Prepared Statements for SQL injections
	- JSTL to prevent XSS
	- CRUD functionality
