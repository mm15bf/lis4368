


> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marcus Mobley 

### Project #2 Requirements:

*Sub-Heading:*

1. MVC Framework
2. Client-side server validation
3. JSTL for XSS attacks
4. CRUD functionality 

#### README.md file should include the following items:

* Screenshots of Client-side Validation



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Client-side Validation*:

![valid user form entry Screenshot](img/userentry.png)

![passed validation Screenshot](img/passedvald.png)

![display data Screenshot](img/displaydata.png)

![modify form Screenshot](img/modifyform.png)

![modified data Screenshot](img/modata.png)

![delete warning Screenshot](img/deletewarning.png)

![database changes Screenshot](img/dbchange.png)

####  Links:

*P2 Links:*
[My Online Portfolio Link](http://localhost:9999/lis4368/customerform.jsp?assign_num=p2 "P2 page")




