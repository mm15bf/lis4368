<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Tue, 02-21-17, 21:58:59 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Marcus Mobley">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - Project1 (Fix course number!)</title>

	<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!--
Bootstrap Integrity and Crossorigin attributes:
http://stackoverflow.com/questions/32039568/what-are-the-integrity-and-crossorigin-attribute
-->

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous" />

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<!-- jQuery DataTables CDN: https://cdn.datatables.net/ //-->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.1.0/css/dataTables.responsive.css"/>

<!-- Form validation styling. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<!-- for main index.jsp file -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- for other assignment files -->
<link href="../css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
		

</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ page import="java.util.Date, java.util.TimeZone, java.text.SimpleDateFormat" %>

<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.jsp" target="_self">Home</a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="../index.jsp">LIS4368</a></li>
					<li><a href="../a1/index.jsp">A1</a></li>
					<li><a href="../a2/index.jsp">A2</a></li>
					<li><a href="../a3/index.jsp">A3</a></li>
					<li><a href="../a4/index.jsp">A4</a></li>
					<li><a href="../a5/index.jsp">A5</a></li>
					<li><a href="../p1/index.jsp">P1</a></li>
					<li><a href="../p2/index.jsp">P2</a></li>
					<li><a href="../test/index.jsp">Test</a></li>					
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<%
//https://docs.oracle.com/javase/6/docs/api/java/text/SimpleDateFormat.html
SimpleDateFormat timeFormat = new SimpleDateFormat("M/d/yy h:m a ");
timeFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
String time = timeFormat.format(new Date());
%>
<%= time %>	
	

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<h1>[Marcus Mobley] My Online Portfolio</h1>

					</div>

					<h2>Customers</h2>
					
					<form id="add_customer_form" method="post" class="form-horizontal" action="#">

						<div class="form-group">
							<label class="col-sm-3 control-label">FName:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="fname" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">LName:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="lname" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Street:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="street" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">City:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="city" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">State:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="state" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Zip:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="zip" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Phone:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="phone" />
							</div>
						</div>						

						<div class="form-group">
							<label class="col-sm-3 control-label">Email:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="email" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Balance:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="balance" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Sales:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="total_sales" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Notes:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" maxlength="255" name="notes" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-5 col-sm-offset-3">
								<button type="submit" class="btn btn-primary" name="signup" value="Sign up">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>

	<%@ page import="java.util.GregorianCalendar, java.util.Calendar" %>
<%  
    GregorianCalendar currentDate = new GregorianCalendar();
    int currentYear = currentDate.get(Calendar.YEAR);
%>
&copy; Copyright <%= currentYear %> My Footer, Inc.

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>

<!-- Turn off client-side validation, in order to test server-side validation. -->
<script type="text/javascript" src="../js/formValidation/formValidation.min.js"></script>

<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="../js/formValidation/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../js/ie10-viewport-bug-workaround.js"></script>
		
 
<script type="text/javascript">
$(document).ready(function() {

	$('#add_customer_form').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {

				fname: {
							validators: {
									notEmpty: {
											message: 'First name required'
									},
									stringLength: {
											min: 1,
											max: 15,
											message: 'First name no more than 15 characters'
									},
									regexp: {
										//http://www.regular-expressions.info/
										//http://www.regular-expressions.info/quickstart.html
										//http://www.regular-expressions.info/shorthand.html
										//http://stackoverflow.com/questions/13283470/regex-for-allowing-alphanumeric-and-space
										//alphanumeric (also, "+" prevents empty strings):
										regexp: /^[a-zA-Z\-]+$/,
										message: 'First name can only contain letters and hyphens.'
									},									
							},
					},

				lname: {
							validators: {
									notEmpty: {
											message: 'Last name required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Last name no more than 30 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z\-]+$/,
										message: 'Last name can only contain letters and hyphens'
									},									
							},
					},
					
					street: {
							validators: {
									notEmpty: {
											message: 'Street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Street no more than 30 characters'
									},
									regexp: {
										//street: only letters,numbers,comma, space character, and period
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,
										message: 'Street  can only contain letters, numbers, commas hyphens, or periods'
									},									
							},
					},
					
					city: {
							validators: {
									notEmpty: {
											message: 'City required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'City no more than 30 characters'
									},
									//cities can contain hypens: Winston-Salem
									regexp: {
										regexp: /^[a-zA-Z0-9\-\s]+$/,
										message: 'City can only contain letters, numbers,hyphens, and special characters(29 Palms)'
									},									
							},
					},
					
					state: {
							validators: {
									notEmpty: {
											message: 'State required'
									},
									stringLength: {
											min: 2,
											max: 2,
											message: 'State must be 2 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z]+$/,
										message: 'State can only contain letters'
									},									
							},
					},
					
					zip: {
							validators: {
									notEmpty: {
											message: 'Zip required, only numbers'
									},
									stringLength: {
											min: 5,
											max: 9,
											message: 'Zip must be 5, and no more than 9 digits'
									},
									regexp: {
										regexp: /^[0-9]+$/,
										message: 'Zip can only contain numbers'
									},									
							},
					},
					
					phone: {
							validators: {
									notEmpty: {
											message: 'Phone required, including area code, only numbers'
									},
									stringLength: {
											min: 10,
											max: 10,
											message: 'Phone must be 10 digits'
									},
									regexp: {
										regexp: /^[0-9]+$/,
										message: 'Phone can only contain numbers'
									},									
							},
					},
					
					
					email: {
							validators: {
									notEmpty: {
											message: 'Email address is required'
									},

									/*
									//built-in e-mail validator, comes with formValidation.min.js
									//using regexp instead (below)
									emailAddress: {
											message: 'Must include valid email address'
									},
									*/
								
									stringLength: {
											min: 1,
											max: 100,
											message: 'Email no more than 100 characters'
									},
									regexp: {
									regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
										message: 'Must include valid email'
									},																		
							},
					},
					
					balance: {
							validators: {
									notEmpty: {
											message: 'Balance required'
									},
									stringLength: {
											min: 1,
											max: 7,
											message: 'Balance can be no more than 6 digits, including decimal point'
									},
									regexp: {
										regexp: /^[0-9\.]+$/,
										message: 'Balance can only contain numbers and decimal point'
									},									
							},
					},
					
					total_sales: {
							validators: {
									notEmpty: {
											message: 'Total sales is required'
									},
									stringLength: {
											min: 1,
											max: 7,
											message: 'Total sales can be no more than 6 digits, including decimal point'
									},
									regexp: {
										regexp: /^[0-9\.]+$/,
										message: 'Total sales can only contain numbers and decimal point'
									},									
							},
					},
			}
	});
});
</script>

</body>
</html>
