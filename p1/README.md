


> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marcus Mobley 

### Project #1 Requirements:

*Sub-Heading:*

1. Client side validation
2. Bitbucket link
3. Bitbucket repo link with assignment files

#### README.md file should include the following items:

* Screenshots of Client-side Validation



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of CLient-side Validation*:

![failed validation Screenshot](img/fail.png)

![passed validation Screenshot](img/pass.png)

####  Links:

*P1 Links:*
[My Online Portfolio Link](http://localhost:9999/lis4368/index.jsp "P1 page")




