> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marcus Mobley 

### Assignment #1 Requirements:

*Sub-Heading:*

1. distributed version control with Git and Bitbucket
2. Java/JSP/Servlet Development installation 
3. Ch. Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above),
* Screenshot of running http://localhost.9999(#2above, Step#4(b) in tutorial),
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. gitinit- Create an empty Git repository or reinitialize an existing one
2. gitstatus-List which files are staged, unstaged, and untracked.
3. gitadd-Add file contents to the index
4. gitcommit-Record changes to the repository
5. gitpush-Update remote refs along with associated objects
6. gitpull-Fetch from and integrate with another repository or a local branch
7. git-diff - Show changes between commits, commit and working tree, etc

#### Assignment Screenshots:

*Screenshot of tomcat running http://localhost*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mm15bf/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mm15bf/myteamquotes/ "My Team Quotes Tutorial")
