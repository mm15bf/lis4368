> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marcus Mobley 

### Assignment #2 Requirements:

*Sub-Heading:*

1. Using static and dynamic web applications
2. MYSQL Development installation 
3. Ch. Questions (Chs 5,6)

#### README.md file should include the following items:

* links to tutorial 2.5 - 2.8
* screenshot of the query results

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 hello Tutorial Link](http://localhost:9999/hello "index.html")

*Tutorial: Request to update a teammate's repository:*
[A2 hellohome Tutorial Link](http://localhost:9999/hello/HelloHome.html "HelloHome.html")

*Tutorial: Request to update a teammate's repository:*
[A2 sayhello Tutorial Link](http://localhost:9999/hello/sayhello "invokes HelloServlet")

*Tutorial: Request to update a teammate's repository:*
[A2 querybook Tutorial Link](http://localhost:9999/hello/querybook.html "querybook.html")

*Tutorial: Request to update a teammate's repository:*
[A2 sayhi Tutorial Link](http://localhost:9999/hello/sayhi "invokes AnotherHelloServlet")

#### Assignment Screenshots:

*Screenshot of tomcat running http://localhost:9999/hello/querybook.html*:

![Tomcat Installation Screenshot](img/querybookresults.png)




