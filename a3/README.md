


> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marcus Mobley 

### Assignment #3 Requirements:

*Sub-Heading:*

1. Database based upon business rules
2. Bitbucket link
3. Chapter Questions (Chs 5,6)
4. Bitbucket repo link with assignment files

#### README.md file should include the following items:

* Screenshots of ERD
* Links to associated files


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots and File Links:

*Screenshot of ERD*:

![ERD Screenshot](img/A3.png)


####  Links:

*A3 Links:*
[A3 MWB Link](https://bitbucket.org/mm15bf/lis4368/src/797f3414c6a4b7015884f30731b92e588051514a/a3/docs/A3.mwb?at=master&fileviewer=file-view-default "A3 mwb")

*Local host webpage link:*
[A3 SQL Link](https://bitbucket.org/mm15bf/lis4368/src/797f3414c6a4b7015884f30731b92e588051514a/a3/docs/A3.sql?at=master&fileviewer=file-view-default "A3 sql")



