


> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marcus Mobley 

### Assignment 4 Requirements:

*Sub-Heading:*

1. Server side validation
2. Bitbucket link
3. Bitbucket repo link with assignment files

#### README.md file should include the following items:

* Screenshots of  Server-side Validation



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Server-side Validation*:

![failed validation Screenshot](img/fail2.png)

![passed validation Screenshot](img/pass2.png)

####  Links:

*A4 Links:*
[My Online Portfolio Link](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4 "A4 page")




